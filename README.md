---
## Input Field Validator - Chrome Extension

*This is a very simple utility to validate every input fields in a webpage in the active Tab. You can get this extension from [Chrome WebStore - Input Field Validator](https://chrome.google.com/webstore/detail/input-field-validator/bdldcibehhfoilmnnakijkaagjibjjid/)*


---